import Link from 'next/link' 

import {api as chefeGioApi} from '../../services/api/recipes'

// Components
import Head from '../../components/head'
import NavBar from '../../components/navbar'
import PageSingleResponse from '../../components/page_single_response'
import BannerHero from '../../components/banner-hero'

// Style
import Style from './style'
import colors from '../../theme/colors'

export default () => {

  function CreateCardList(recipes){
    const keys = Object.keys(recipes)
    return keys.map(k =>  CreateCard(recipes[k]))
  } 

  function CreateCard(recipe) {
    return (
      <article className="card-item" key={recipe.id}>
        <Link href={`/receitas/[id]`} as={`/receitas/${recipe.id}`}>
        <a>
          <figure className="image-container">
            <img src={recipe.thumbnail} alt={recipe.title}/>
          </figure>
          <div className="content-container">
            <strong className="title">{recipe.title}</strong>
            <p>{recipe.description}</p>
          </div>
        </a>
        </Link>
      </article>
    )
  }

  return (
    <Style className="container page-list">
      <Head title={'Chefe Gio - Receitas'} />
      
      <header>
        <NavBar />
        <img className="logo" src="/uploads/brand/chefe-gio-name-icon-tranparency.png" alt="Chefe Gio Logo"/>
        <h3 className="title title-page">Receitas</h3>

        <BannerHero 
          imageUrl='/uploads/recipe/banner.jpg'
          bannerHeroStyle={{
            height: '40rem',
            backgroundAttachment: 'fixed'
          }}
          overlayStyle={{backgroundColor: `${colors.darkGray}d6`}} 
        />

      </header>
      
      <main>
        <h4 className="description">O que vou escolher para fazer hoje...</h4>
        
        <PageSingleResponse 
          api={chefeGioApi}
          functions={{
            Content: CreateCardList
          }}
          className="page-list-content"
        />
      </main>
    </Style>
  )
}