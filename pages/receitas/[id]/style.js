import styled from 'styled-components'
import colors from '../../../theme/colors'

export default styled.section`

  .cold-brew .banner-hero {
    background-position: center -30vw !important;
  }

  @media (min-width: 700px) {
    .cold-brew .banner-hero {
      background-position: center -40rem !important;
    }
  }

  @media (min-width: 1100px) {
    .cold-brew .banner-hero {
      background-position: center 79vw !important;
    }
  }

`