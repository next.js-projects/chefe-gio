import { useEffect, useState } from 'react' 

import {api as chefeGioApi} from '../../../services/api/recipes'
import router from 'next/router'

// Style
import Style from './style'

// Components
import Head from '../../../components/head'
import NavBar from '../../../components/navbar'
import PageSingleResponse from '../../../components/page_single_response'
import BannerHero from '../../../components/banner-hero'

export default () => {
  const [routerQuery, setRouterQuery] = useState({})
  const [pageTitle, setPageTitle] = useState('Detalhe da Receita')

  const bannerStyle = {
    // top: '4rem',
    height: '40rem',
    backgroundAttachment: 'fixed',
    backgroundPosition: 'center',
  }

  useEffect(() => {
    setRouterQuery(router.query)
    setPageTitle(chefeGioApi[router.query.id].title)
  }, [])

  function CreateRecipe(recipe) {
    return (
      <article className={`page-detail-article ${recipe.id}`}>
        {/* <img src={recipe.thumbnail} /> */}
        <BannerHero 
          imageUrl={recipe.thumbnail}
          bannerHeroStyle={bannerStyle}
        />
        {/* {recipe.images.map((image, id) => <img src={image} key={id} />)} */}
        <main>
          <h3 className="title">{recipe.title}</h3>
          <section className="content">
            <p>{recipe.description}</p>

            <label forhtml="ingredients">Ingredientes:</label>
            <p id="ingredients">{recipe.ingredients}</p>

            <label forhtml="how_to_prepare">Como Preparar:</label>
            <p id="how_to_prepare">{recipe.how_to_prepare}</p>
          </section>

        </main>
      </article>
    )
  }

  return (
    <Style className="container page-detail" bannerStyle={bannerStyle} >
      <Head title={`Chefe Gio - ${pageTitle}`} />
      <NavBar backUrl="receitas" />
      <PageSingleResponse 
        api={chefeGioApi[routerQuery.id]}
        functions={{
          Content: CreateRecipe
        }}
      />
    </Style>
  )
}