import Layout from '../components/layout'

import ThemeContextProvider from '../services/contexts/theme'

export default function App({ Component, pageProps }) {
  return (
    <ThemeContextProvider>
      <Layout><Component {...pageProps} /></Layout>
    </ThemeContextProvider>
  )
}