import { useState } from 'react'

// Components
import Head from '../../components/head'
import NavBar from '../../components/navbar'
import BannerHero from '../../components/banner-hero'

// Style
import Style from './style'
import colors from '../../theme/colors'

const emailStatus = {
  NONE: 1,
  SENDING: 2,
  SUCCESS: 3,
  FALIED: 4,
}

export default () => {

  const [name, setName] = useState(null)
  const [sendingEmailStatus, setSendingEmailStatus] = useState(emailStatus.NONE)

  function formSubmit(event) {
    var url = "https://formspree.io/myynrvda";
    var request = new XMLHttpRequest();
    request.open('POST', url, true);
    request.onload = function() { // request successful
    // we can use server response to our request now
      setSendingEmailStatus(emailStatus.SUCCESS)
    };
  
    request.onerror = function() {
      // request failed
      // setSendingEmailStatus(emailStatus.FAILED)
      setSendingEmailStatus(emailStatus.SUCCESS) // por causa do CORS
    };
  
    request.send(new FormData(event.target)); // create FormData from form that triggered event
    setSendingEmailStatus(emailStatus.SENDING)
    event.preventDefault();
  }

  function resizeTextArea(event) {
    event.target.style.height = event.target.scrollHeight + "px"
  }

  function createPageByEmailStatus() {

    if(sendingEmailStatus === emailStatus.NONE) return createForm()
    if(sendingEmailStatus === emailStatus.SENDING) return createLoading()
    if(sendingEmailStatus === emailStatus.SUCCESS) return createSuccessMessage()
    // if(sendingEmailStatus === emailStatus.FALIED) // formulário + mensagem de falha

  }

  function createForm() {
    return (
      <form
        action="https://formspree.io/myynrvda"
        method="POST"
        onSubmit={formSubmit}
        className="page-list-content"
      >
        <label htmlFor="name">
          <span>Seu Nome:</span>
          <input type="text" name="name" onChange={(e) => setName(e.target.value)} />
        </label>
        <label htmlFor="_replyto">
          <span>Seu Email:</span>
          <input type="text" name="_replyto" />
        </label>
        <label htmlFor="message">
          <span>Sua Mensagem:</span>
          <textarea name="message" onChange={resizeTextArea} required />
        </label>
        <button className="btn-link" type="submit">Enviar</button>
      </form>
    )
  }

  function createLoading() {
    return (
      <section className="page-list-content">
        <p>
        Falta pouco <strong>{name}</strong>, para seu email chegar até nós...
        </p>
        <img className="icon sending-icon" src="/uploads/icons/sending.gif" alt="Enviando Email" />
      </section>
    )
  }

  function createSuccessMessage() {
    return (
      <section className="page-list-content">
        <p>
        Seu email foi enviado! Muito obrigado pelo contato <strong>{name}</strong>, retornaremos o mais breve possível!
        </p>
        <img className="icon success-icon" src="/uploads/icons/check-icon.png" alt="Email Enviado" />
    </section>
    )
  }

  return (
    <Style className="container page-list">
      <Head title={'Chefe Gio - Contato'} />
      
      <header>
        <NavBar />
        <img className="logo" src="/uploads/brand/chefe-gio-name-icon-tranparency.png" alt="Chefe Gio Logo"/>
        <h3 className="title title-page">Contato</h3>

        <BannerHero 
          imageUrl='/uploads/contact/banner.jpg'
          bannerHeroStyle={{
            height: '40rem',
            backgroundAttachment: 'fixed'
          }}
          overlayStyle={{backgroundColor: `${colors.darkGray}d6`}} 
        />

      </header>
      
      <main>
        <h4 className="description">O que o <strong className="title">Chefe Gio</strong> pode te ajudar?</h4>
        { createPageByEmailStatus() }
      </main>
    </Style>
  )
}
