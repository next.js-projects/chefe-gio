import styled from 'styled-components'

import colors from '../../theme/colors'

export default styled.section`

  .description {
    display: flex;
    justify-content: center;
    align-items: center;

    .title {
      margin: 0 1rem 0 1.6rem;
      font-size: 1em;
    }
  }

  form {
    display: grid !important;
    padding: 1rem !important;
    grid-template-columns: 1fr;
  }

  .page-list-content:before {
    content: '';
    position: fixed;

    bottom: 0;
    left: 0;
    width: 100%;
    height: 85vh;
    background-position: center;
    background-image: url(/uploads/contact/sendmail.jpg);
    background-size: cover;
    opacity: 0.025;
    -webkit-filter: grayscale(1);
    filter: grayscale(1);
    z-index: -100;
  }

  .page-list-content {
    display: grid !important;
    justify-items: center;
    padding: 1rem !important;
    text-align: center;

    p {
      color: white;
      font-size: 1.6rem;
      margin: 1.6rem auto;
    }
  }

  .icon {
    height: 20rem;
    width: 20rem;
  }

  .sending-icon {
    border-radius: 100%;
    box-shadow: inset 0px 0px 1px 1px ${colors.darkRed};
  }

  form.page-list-content  {

    label {
      display: grid !important;
      margin: 1.6rem auto;
      max-width: 50rem;
      width: 100%;
    }
  
    span {
      color: ${colors.white};
      font-size: 1.2rem;
  
      background: ${colors.darkRed};
      padding: .5rem;
      margin: 0 .5rem 0 .5rem;
      border-bottom: 1px solid #00000021;
      border-radius: 5px 5px 0 0;
      text-align: center;
    }
  
    input, textarea
    {
      font-size: 1.6rem;
      padding: 0.5rem 1rem;
      background: ${colors.darkRed} !important;
      outline: none;
      border: none;
      box-shadow: -1px 1px 1px 0px #0000009e !important;
      border-radius: 5px;
      color: ${colors.white} !important;
      -webkit-text-fill-color: ${colors.white} !important;

      text-align: center;
  
      font-family: Alegreya Sans;

      -webkit-box-shadow: 0 0 0px 500px ${colors.darkRed} inset !important;
      transition: background-color 5000s ease-in-out 
    }

    input:-webkit-autofill {
      -webkit-box-shadow: 0 0 0px 15px ${colors.darkRed} inset !important;
    }

    textarea:-webkit-autofill {
      -webkit-box-shadow: 0 0 0px 500px ${colors.darkRed} inset !important;
    }
  
    textarea {
      text-align: left;
      resize: none;
      min-height: 10rem;
    }
  
    .btn-link {
      outline: none;
      border: none;
      width: 100%;
      margin: 2rem auto;
  
      max-width: 20rem;
      font-weight: bold;
      letter-spacing: 0.5rem;
      font-family: Alegreya sans;
    }

  }

`
