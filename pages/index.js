import DefaultPage from '../components/default_page'

export default () => {
  return (
    <DefaultPage 
      message01="Seja muito bem-vindo ao"
      message02="aqui tudo é feito com muito amor!"
    />
  )
}