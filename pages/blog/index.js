import Link from 'next/link' 

import { createClient } from "contentful"
import config from "../../config.json"

// Components
import Head from '../../components/head'
import NavBar from '../../components/navbar'
import PageSingleResponse from '../../components/page_single_response'
import BannerHero from '../../components/banner-hero'

// Style
import Style from './style'
import colors from '../../theme/colors'

// Utils
import slugfy from '../../utils/slugfy'

// Instantiate the app client
const client = createClient({
  space: config.space,
  accessToken: config.accessToken
})

const Blog = (props) => {

  function CreateCardList(allPosts){
    const keys = Object.keys(allPosts)
    return keys.map(k =>  CreateCard(allPosts[k]))
  } 

  function CreateCard(post) {

    const {sys, fields} = post
    const urlPost = slugfy(post.fields.title)

    return (
      <article className="card-item" key={sys.id}>
        <Link href={`/blog/[id]`} as={`/blog/${urlPost}`}>
        <a>
          <figure className="image-container">
            <img src={fields.image.fields.file.url} alt={fields.image.fields.title}/>
          </figure>
          <div className="content-container">
            <p>{fields.title}</p>
          </div>
        </a>
        </Link>
      </article>
    )
  }

  return (
    <Style className="container page-list">
      <Head title={'Chefe Gio - Blog'} />
      
      <header>
        <NavBar />
        <img className="logo" src="/uploads/brand/chefe-gio-name-icon-tranparency.png" alt="Chefe Gio Logo"/>
        <h3 className="title title-page">Blog</h3>

        <BannerHero 
          imageUrl='/uploads/blog/banner.jpg'
          bannerHeroStyle={{
            height: '40rem',
            backgroundAttachment: 'fixed',
            backgroundPositionX: 'center'
          }}
          overlayStyle={{backgroundColor: `${colors.darkGray}d6`}} 
        />

      </header>
      
      <main>
        <h4 className="description">Inspire-se antes do preparo...</h4>
        
        <PageSingleResponse 
          api={props.allPosts}
          functions={{
            Content: CreateCardList
          }}
          className="page-list-content"
        />
      </main>
    </Style>
  )
}

Blog.getInitialProps = async () => {
  const entries = await client.getEntries({
    content_type: "chefeGioArticle",
    order: "-sys.createdAt"
  });

  return { allPosts: entries.items };
};

export default Blog;