import { useEffect, useState } from 'react' 

import router from 'next/router'

import { createClient } from "contentful"
import config from "../../../config.json"

// Style
import Style from './style'

// Components
import Head from '../../../components/head'
import NavBar from '../../../components/navbar'
import PageSingleResponse from '../../../components/page_single_response'
import BannerHero from '../../../components/banner-hero'

// Instantiate the app client
const client = createClient({
  space: config.space,
  accessToken: config.accessToken
})

const PostDetail = (props) => {
  const [routerQuery, setRouterQuery] = useState({})
  const [pageTitle, setPageTitle] = useState('Conteúdo do Post')
  const [post, setPost] = useState({})

  const bannerStyle = {
    // top: '4rem',
    height: '40rem',
    backgroundAttachment: 'fixed',
    backgroundPosition: 'center',
  }

  useEffect(() => {
    setRouterQuery(router.query)
  }, [])

  useEffect(() => {
    getPostDetailById()
  }, [routerQuery])

  useEffect(() => {
    post.fields && setPageTitle(post.fields.title)
  }, [post.fields])

  const getPostDetailById = async () => {
      const entries = await client.getEntries({
        content_type: "chefeGioArticle",
        'fields.slug': routerQuery.id,//'cozinhar-e-um-modo-de-amar-os-outros'
      });
    
      setPost(entries.items[0])
  }

  function CreatePost(myPost) {

    if(!myPost.fields) {
      const fakeParagraph = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam at diam libero. Curabitur eget posuere purus. Etiam sodales sapien eget leo suscipit, et accumsan ante lobortis."

      return (
        <article className={`page-detail-article loading`}>
          <BannerHero 
            imageUrl={""}
            bannerHeroStyle={bannerStyle}
            className="loading-item"
          />
          <main>
            <h3 className="title loading-item">Lorem Ipsum</h3>
            <section className="content">
              {[0,1,2,3].map((p, i) => <p key={i} className="loading-item">{fakeParagraph}</p>)}
            </section>
          </main>
        </article>
      )
    }

    const {sys, fields} = myPost

    return (
      <article className={`page-detail-article ${sys.id}`}>
        <BannerHero 
          imageUrl={fields.image.fields.file.url}
          bannerHeroStyle={bannerStyle}
        />
        <main>
          <h3 className="title">{fields.title}</h3>
          <section className="content">
            {fields.content.content.map((c, i) => <p key={i}>{c.content[0].value}</p>)}
          </section>
        </main>
      </article>
    )
  }

  return (
    <Style className="container page-detail" bannerStyle={bannerStyle} >
      <Head title={`Chefe Gio - ${pageTitle}`} />
      <NavBar backUrl="blog" />
      <PageSingleResponse 
        api={post}
        functions={{
          Content: CreatePost
        }}
      />
    </Style>
  )
}

export default PostDetail;