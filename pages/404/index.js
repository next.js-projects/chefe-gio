import DefaultPage from '../../components/default_page'

export default () => {
  return (
    <DefaultPage 
      message01="A página que está procurando não existe no"
      message02="Que tal tentar um dos links abaixo..."
    />
  )
}
