import React, { useState, createContext } from 'react'

export const ThemeContext = createContext()

export default ({ children }) => {

  const [theme, setTheme] = useState('main')

  const toogleTheme = () => setTheme(theme === 'main' ? 'dark' : 'main')

  return (
    <ThemeContext.Provider 
      value={{theme, toogleTheme, setTheme}}
    >
      {children}
    </ThemeContext.Provider>
  )
}