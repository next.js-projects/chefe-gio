import axios from 'axios'

const route = 'api/'
export const baseURL = 
  (process.env.NODE_ENV === 'production' ? 
    'https://chefe-gio.vercel.app/' : 'http://localhost:3000/') + route
export const api = axios.create({baseURL})