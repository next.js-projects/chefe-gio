import bolo_cafe from './_bolo_de_cafe'
import pao_integral from './_pao_integral'
import hamburguer from './_hamburguer'
import sufle_abobrinha from './_sufle_abobrinha'
import cold_brew from './_cold_brew'

const recipes = [
  bolo_cafe,
  cold_brew,
  hamburguer,
  pao_integral,
  sufle_abobrinha,
]

let auxAPI = {}
recipes.forEach(r => auxAPI[r.id] = r)

export const api = auxAPI
export const baseURL = '/uploads/recipes/'