import recipePattern from './_default'

export default recipePattern({
  title: 'Bolo de Café',
  description: 'Prático e delicioso bolo de café, perfeito para um café da tarde! E o melhor... é que ele não vai farinha!',
  ingredients: '- 6 ovos<br />- 2 colheres de manteiga<br />- 8 colheres de açúcar mascavo<br />- 4 colheres de cacau ou chocolate em pó<br />- 4 colheres de café em pó<br />- 100 g de coco ralado<br />- 1 colher de fermento em pó',
  how_to_prepare: '1. Bata os ingredientes da massa no liquidificador.<br />2. Despeje em uma forma untada e enfarinhada.<br />3. Leve ao forno médio, pré-aquecido, por aproximadamente 30 minutos, ou até que furando com um palito ele saia limpo.<br />4. Reserve.<br /><br />Como opção para a massa ficar com mais sabor do café, ao final do processo, faça pequenos furinhos na massa e despeje um pouco de café coado.<br /><br />Minha sugestão é fazer uma cobertura deliciosa, como um chocolate meio amargo derretido, ou mesmo uma cobertura usando o próprio café. Dê asas a sua imaginação e vontade!',
  images: [],
  thumbnail: 'bolo_cafe.png',
})