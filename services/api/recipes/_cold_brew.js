import recipePattern from './_default'

export default recipePattern({
  title: 'Cold Brew',
  description: 'Café gelado muito saboroso para refrescar os dias quentes!',
  ingredients: '- 4 xícaras de água<br />- 1 xícara de pó de café<br />- Raspas da casca de limão siciliano (pode ser de laranja, ou outra fruta cítrica)<br />- Cubos de gelo<br />- Açúcar à gosto<br />',
  how_to_prepare: '1. Coloque em uma jarra o pó de café.<br />2. Despeje a água e vá misturando, para o café ir ficando úmido e manter-se no fundo.<br />3. Coloque a jarra tampada, com a mistura na geladeira. Deixe na geladeira por 12 horas e mexa a água algumas vezes, durante esse tempo.<br />4. Após esse período, coe o café e em cada copo, raspe um pouco da casca da fruta cítrica e coloque gelo. Está pronto seu Cold Brew, aproveite!<br /><br />Como uma sugestão, junto com as raspas da fruta, deixe um pouco do suco na boca do copo, acentuando um pouco mais o sabor da fruta.',
  images: [],
  thumbnail: 'cold_brew.jpeg',
})