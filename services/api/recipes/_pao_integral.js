import recipePattern from './_default'

export default recipePattern({
  title: 'Pão Integral',
  description: 'Nada como um pão caseiro integral recheado... é fácil de fazer, basta colocar a mão na massa!',
  ingredients: '- 1kg de farinha integral<br />- 4 colheres de açucar mascavo<br />- 1 colher rasa de sal<br />- 2 1/2 xícaras de leite<br />- 3 ovos<br />- 50gr fermento de pão<br />- 1 colher de manteiga',
  how_to_prepare: '1. Em uma bacia grande, coloque todos os ingredientes secos (farinha, açucar, sal, fermento) e misture.<br />2. No liquidificador, misture os ingredientes molhados (leite, ovos, manteiga).<br />3. Misture o que estava no liquidificador na bacia, usando as mãos até a massa parar de grudar.<br />4. Leve a massa de pão para algum lugar quente, pode envolver a bacia com um edredon, por exemplo, deixando a massa crescer (em torno de 2hrs à 3hrs). <br /><b>Dica: para saber melhor o tempo de crescimento, coloque uma bolinha da massa em um copo com água, quando a mesma subir, a massa está boa</b><br />5. Antes de levar ao forno, podemos preparar algum recheio para a massa. Coloque o recheio no meio da massa esticada e vá enrolando. O recheio que fiz foi de linguiça e ficou divino!<br />6. Leve a massa ao forno pré-aquecido e deixe em torno de 25 minutos e no forno alto.<br /><br />De qualquer modo, fique sempre de olho no seu pão e "escute o cheiro" dele pois assim não vai ter erro, tenho certeza que você e sua fámilia vão desfrutar de um delicioso alimento!',
  images: [],
  thumbnail: 'bread.png',
})