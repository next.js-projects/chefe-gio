import recipePattern from './_default'

export default recipePattern({
  title: 'Hamburguer Caseiro',
  description: 'Nada melhor que fazer seu próprio hamburguer! Você nunca mais vai querer qualquer hamburguer.',
  ingredients: '- 1kg de carne moída (fiz com Acém)<br />-  2 cabeças de alho<br />-  Bacon à gosto<br />-  2 colheres de sopa rasas de sal<br />-  Azeite ou manteiga',
  how_to_prepare: '1- Com a carne moída, junte e misture o alho picadinho, o bacon, o sal e o azeite. Vai precisar colocar a "mão na massa"!<br />2- Faça discos de aproximadamente 125 gramas cada um.<br />3- Se possível, deixar em torno de 1 à 2 horas a carne para descansar e pegar melhor o gosto dos temperos. É sútil, mas vale a pena se tiver esse tempo e paciência.<br />4- Coloque os discos de carne em uma assadeira (untada ou com papel manteiga), leve ao forno pré aquecido e em torno de uns 25 minutos a carne estará bem cozida, com uma casquinha por fora e suculenta por dentro.<br /><br />Dica do chefe: esse hamburguer é um coringa para qualquer tipo de refeição, arroz com feijão, com macarrão e é claro em um delicioso lanche.',
  images: ['hamburguers.jpeg'],
  thumbnail: 'hamburguer.jpeg',
})