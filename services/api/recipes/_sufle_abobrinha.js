import recipePattern from './_default'

export default recipePattern({
  title: 'Suflê de Abobrinha',
  description: 'Suflê de Abobrinha Fit, feito com farinha integral.',
  ingredients: '- 3 abobrinhas grandes<br />- 4 ovos inteiros<br />- 1 xícara de chá de farinha de integral<br />- 1/2 xícara de manteiga<br />- 1 cebola pequena, picadinha<br />- 3 colheres de sopa de queijo ralado<br />- sal e orégano a gosto<br />- salsinha picada a gosto<br />- 1 colher de sopa de fermento em pó',
  how_to_prepare: '1. Em uma tigela rale as abobrinhas, passando no ralo grosso.<br />2. Acrescente os ovos, a farinha, a manteiga e o sal.<br />3. Misture bem.<br />4. Por último, acrescente o orégano, o queijo ralado e o fermento em pó.<br />5. Mexa delicadamente.<br />6. Leve ao forno quente em forma untada e enfarinhada.<br />7. Asse até ficar dourada.<br /><br />Para incrementar sua receita, quando estiver faltando uns 5 minutos para chegar ao ponto , sugiro fazer uma camada de requeijão,  colocar umas fatias de queijo em cima e rodelas de tomate. ',
  images: [],
  thumbnail: 'screenshot_9.png',
})