// Id: string
// Title: string short - required
// Description: string long
// Ingredients: string long <HTML> - required
// How To Prepare : string long <HTML> - required
// Images: Array Image paths
// Thumbnail: Image path

import stringToHTML from 'html-react-parser'

export default ({id, title, description, ingredients, how_to_prepare, images, thumbnail}) => {
  return {
    ['id']: id || generateId(title),
    title, 
    description, 
    ['ingredients']: stringToHTML(ingredients), 
    ['how_to_prepare']: stringToHTML(how_to_prepare), 
    ['images']: setArrayMediaPath(images),
    ['thumbnail']: setMediaPath(thumbnail),
  }
}

const generateId = (title) => 
  title
  .normalize('NFD')
  .replace(/[\u0300-\u036f]/g, '')
  .replace(/[^\w\-]+/g, '-')
  .toLowerCase()

const recipesPath = '/uploads/recipes/'

const setArrayMediaPath = (medias) => medias ? medias.map(media => setMediaPath(media)) : []
const setMediaPath = (name) => name && (recipesPath + name)