import styled from 'styled-components'

export default styled.section`
    position: absolute;
    left: 0;
    top: 0;
    height: 100vh;
    width: 100vw;
    z-index: -1;

    background-size: cover;
    background-image: url(${props => props.image});
    
    .overlay {
      height: inherit;
      width: inherit;
    }
`