import Link from 'next/link' 

// Icons
import { GoHome } from "react-icons/go";
import { TiArrowBackOutline } from "react-icons/ti";

TiArrowBackOutline
// Styles
import Style from './style'

// Components
import MenuLinks from '../menu_links'

export default ({backUrl}) => {

  const historyBack = (e) => {
    e.preventDefault()
    history.back()
  }

  return (
    <Style className="container">
      <ul>
        <li className="btn-back">
          <Link href={'/' + backUrl} >
            <a className="btn-link home-icon" onClick={(e) => !backUrl && historyBack(e)}><TiArrowBackOutline /></a>
          </Link>
        </li>
        <li className="btn-home">
          <Link href="/"><a className="btn-link home-icon"><GoHome /></a></Link>
        </li>
        <MenuLinks />
      </ul>
    </Style>
  )
}
