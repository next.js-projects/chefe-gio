import styled from 'styled-components'

export default styled.nav`
  && {
    position: fixed;
    width: 100vw;
    justify-content: center;
    top: 0;
    left: 0;
    display: grid;
    grid-template-columns: repeat(4,auto);
    z-index: 100;
    left: 50%;
    transform: translate(-50%);
  }

  ul {
    display: flex;
  }

  li {
    align-items: center;
    display: flex;
  }

  .btn-back {
    position: absolute;
    right: 1rem;
  }

  .btn-home {
    position: absolute;
    left: 1rem;
  }

  .home-icon {
    padding: 0 1rem;

    > * {
      font-size: 3rem;
    }
  }
`