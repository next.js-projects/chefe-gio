import { useRouter } from 'next/router'
import Link from 'next/link' 

// Configs
import config from '../../services/api/menu_config'

export default () => {

  const router = useRouter()
  const selected = router.route.slice(1)

  return (
    <>
    {
      config.map(item => 
        <li key={item.url}>
          <Link href={`/${item.url}`}>
            <a 
              className={`btn-link ${selected === item.url && 'selected'} ${item.disabled && 'disabled'}`}>
                {item.label}
              </a>
          </Link>
        </li>
      )
    }
    </>
  )
}
