import { useEffect, useState } from 'react'

export default ({api, route, functions: {Loading, ErrorMsg, Content, WithoutResponse}, className}) => {

  const [response, setResponse] = useState([])
  const [error, setError] = useState(null)
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    api.get(route)
    .then(response => setResponse(response.data))
    .catch(err => setError(err))
    .finally(() => setLoading(false))
  }, [])
  
  function CreateContent() {
    if(loading) return Loading ? Loading() : <div>Loading...</div>
    if(error) return ErrorMsg ? ErrorMsg() : <div>{error}</div>

    if(response) return (Content && Content(response)) || <div>{response}</div>
    else return (WithoutResponse && WithoutResponse()) || <div>Without data in response.</div>
  }

  return <div className={className}>{CreateContent()}</div>

}