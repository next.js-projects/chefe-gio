// Components
import Head from '../../components/head'
import MenuLinks from '../../components/menu_links'
import BannerHero from '../../components/banner-hero'

import Link from 'next/link'

// Styles
import Style from './style'
import colors from '../../theme/colors'

// Utils
import stringToHTML from 'html-react-parser'

export default ({message01, message02}) => {
  return (
    <Style className="container">
      <Head />

      <main>
        <Link href='/'><a>
          <figure className="main-logo">
            <img src="/uploads/brand/chefe-gio-logo.png" alt="Chefe Gio Logo" />
          </figure>
        </a></Link>

        <BannerHero 
          imageUrl='/uploads/home/bread_hands.jpg'
          bannerHeroStyle={{backgroundPositionY: '60%'}}
          overlayStyle={{backgroundColor: `${colors.darkGray}66`}} 
        />

        <section className="main-content">
        
          <div className="overlay">
            <div className="overlay overlay-desktop" />
            <em>{stringToHTML(message01)} <h1 className="title">Chefe Gio</h1> {stringToHTML(message02)}</em>
          </div>
          <nav>
            <ul>
              <MenuLinks />
            </ul>
          </nav>
        </section>
      </main>
    </Style>
  )
}
