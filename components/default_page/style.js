import styled from 'styled-components'
import colors from '../../theme/colors'

export default styled.div`

  main {
    overflow: hidden;
    height: 100vh;
    text-align: center;

    display: grid;
    justify-items: center;
  }

  .main-logo {
    width: 22.5rem;

    img {
      border: 10px solid ${colors.red};
      border-radius: 0 0 5px 5px;
      box-shadow: 0px 0px 2px 2px ${colors.darkGray};
    }
  }

  .main-content {
    width: 100%;

    font-size: 2rem;
    padding-top: 3rem;

    .overlay {
      background-color: ${colors.darkGray}bb;
      color: ${colors.gray};
      padding: 10px 0;
    }

    nav {
      padding: 2rem;

      li {
        display: grid;
        margin: 5px;
      }

    }
  }

  @media (min-width: 700px) {
    .main-content {
      max-width: 50rem;
      padding: 5rem;

      nav {
        padding: 4rem 0;
      }

      ul {
        display: flex;
        justify-content: space-evenly;
      }

      li {
        display: list-item;
      }
    }

    .overlay-desktop {
      display: block !important;
      position: absolute;
      padding: 0;
      width: 100%;
      height: 140px;
      left: 0;
      z-index: -1;
    }
  }
`
