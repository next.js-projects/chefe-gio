import { useEffect } from 'react'

// Context
import { useContext } from 'react'
import { ThemeContext } from '../services/contexts/theme'

// Style Components
import { ThemeProvider } from "styled-components"
import GlobalStyle from "../theme/globalStyle";
import ClassesGlobalStyles from "../theme/classes"
import FontGlobal from '../theme/fonts'

export default ({ children }) => {

  useEffect(() => {
    const nextBtn = document.querySelector('#__next-prerender-indicator')
    nextBtn && nextBtn.remove()
  }, [])

  const context = useContext(ThemeContext)

  return (
    <ThemeProvider theme={{ name: context.theme }}>
      { children }
      { ClassesGlobalStyles.map((ClassesGlobalStyle, index) => <ClassesGlobalStyle key={index} />) }
      <GlobalStyle />
      <FontGlobal.Faces />
    </ThemeProvider>
  )
}
