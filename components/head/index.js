import Head from 'next/head'

export default ({title, children}) => {
  return (
    <Head>
      <title>{title || 'Chefe Gio'}</title>
      <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-icon-180x180.png" />
      
      <link rel="apple-touch-icon" sizes="152x152" href="/favicons/apple-icon-152x152.png" />
      <link rel="icon" type="image/png" sizes="192x192"  href="/favicons/android-icon-192x192.png" />

      <meta name="msapplication-TileColor" content="#ffffff" />
      <meta name="msapplication-TileImage" content="/favicons/ms-icon-144x144.png" />
      <meta name="theme-color" content="#ffffff" />

      {children}
    </Head>
  )
}