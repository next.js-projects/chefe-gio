import { createGlobalStyle } from "styled-components";

import colors from "../colors";

export default createGlobalStyle`

  .page-list {

    header {
      display: flex;
      align-items: center;
      justify-content: center;
      height: 40rem;
    }
  
    .logo {
      width: 15rem;
      margin: 1rem 0 0 1rem;
      z-index: 1;
    }
  
    .title-page {
      color: ${colors.white};
  
      border-radius: 5px;
      padding: 0 3rem 0 5rem;
      margin-left: -2.5rem;
      background-color: ${colors.red};
    }
  
    .description {
      text-align: center;
      padding: 2rem 1rem;
      font-size: 2rem;
      color: ${colors.white};
      box-shadow: 0px 2px 2px 2px ${colors.darkGray}40;
    }
  
    .page-list-content {
      padding: 1rem 0;
      background-color: ${colors.darkGray}2e;
    }
  
    @media (min-width: 600px) {
      .page-list-content {
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
      }
  
      .page-list-content article {
        width: 27.5rem;
      }
    }

  }

`