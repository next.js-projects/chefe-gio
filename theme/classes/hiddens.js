import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`

  [class*='-desktop'] {
    display: none;  
  }

  #__next-prerender-indicator, {
    display: none !important;
  }

`