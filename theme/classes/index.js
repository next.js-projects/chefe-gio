import buttons from './buttons'
import texts from './texts'
import hiddens from './hiddens'
import cardItem from './card-item'
import pageList from './page-list'
import pageDetail from './page-detail'
import loading from './loading'

export default [
  buttons,
  texts,
  hiddens,
  cardItem,
  pageList,
  pageDetail,
  loading,
]