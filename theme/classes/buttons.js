import { createGlobalStyle } from "styled-components";

import colors from "../colors";

export default createGlobalStyle`

  a { text-decoration: none; cursor: pointer; }

  .btn-link {
    padding: 10px 15px;
    background-color: ${colors.red};
    color: ${colors.gray};
    border-radius: 5px;
    box-shadow: 1px 1px 1px 1px ${colors.darkGray}aa;

    transition: .25s;
  }

  .btn-link:hover {
    background-color: ${colors.gray};
    color: ${colors.red};
  }

  .btn-link.disabled {
    cursor: default;
    pointer-events: none;
    color: ${colors.white}7a;
    background-color: ${colors.gray};
  }

  .btn-link.selected {
    cursor: default;
    pointer-events: none;
    color: ${colors.yellow};
    background-color: ${colors.red};
  }

`