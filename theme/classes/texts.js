import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`

  :root {
    font-size: 60%;
    font-family: Alegreya Sans;
  }

  @media (min-width: 700px) {
    :root {
      font-size: 62.5%;
    }
  }

  .title {
    font-family: Amsterdam Two;
  }

  h3 {
    font-size: 3rem;
  }

  em {
    font-style: normal;
  }

`