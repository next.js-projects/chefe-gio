import { createGlobalStyle } from "styled-components";

import colors from "../colors";

export default createGlobalStyle`

.card-item {
  margin: 1rem;

  font-size: 1.6rem;
  text-align: center;

  border-radius: 5px;
  box-shadow: 2px 2px 1px 1px ${colors.darkGray}85;

  padding: 0 0 1rem 0;

  background-color: ${colors.white};
  strong, p { 
    color: ${colors.red};
  }

  p {
    padding: 1.6rem 1rem 0 1rem;
  }

  .image-container {
    width: 100%;
    height: 30rem;
    overflow: hidden;
    display: flex;
    align-self: center;
    justify-self: center;

    box-shadow: 0px 0px 10px 1px ${colors.darkGray}a3;
    border-radius: 5px 5px 0 0;
  }

  img {
    max-width: none;
    min-width: 100%;
    min-height: 100%;
    align-self: center;
  }

  .content-container {
    padding: 1.2rem 1rem;
  }

  .title {
    letter-spacing: .5rem;
    color: ${colors.white};
    background-color: ${colors.red};
    padding: .5rem 1rem;
    border-radius: 5px;
  }

}

`