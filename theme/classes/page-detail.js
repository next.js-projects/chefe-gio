import { createGlobalStyle } from "styled-components";

import colors from "../colors";

const bannerStyle = {
  // top: '4rem',
  height: '40rem',
  backgroundAttachment: 'fixed',
  backgroundPosition: 'center',
}

export default createGlobalStyle`

  .page-detail {

    display: flex;
    justify-content: center;
  
    main {
      position: relative;
      top: calc(${bannerStyle.height || '0px'} + ${bannerStyle.top || '0px'});
    
      color: ${colors.white};
      font-size: 1.6rem;
    }
  
    .title {
      text-align: center;
      padding: 2rem;
  
      box-shadow: 0px 2px 2px 2px ${colors.darkGray}9e;
      background-color: ${colors.darkGray}29;
    }
  
    .page-detail-article .content {
      padding: 2rem;
      background-color: ${colors.darkGray}9e;
      max-width: 60rem;
  
      label {
        font-weight: bold;
        font-size: 2rem;
        margin-top: 1rem;
      }
  
      p {
        margin: 1.5rem;
        font-size: 1.8rem;
      }
  
      p:not(:last-child) {
        margin: 1.5rem 0 3.5rem 1.5rem;
      }
  
      br {
        height: 3rem !important;
        line-height: 3rem !important;
        content: " ";
        
        font-size: 3rem;
        display: block;
      }
    }

  }

`