import { createGlobalStyle } from "styled-components";

import colors from "../colors";

export default createGlobalStyle`

.loading {
  .loading-item {
    animation: bg-animation 1s ease infinite;
    color: #ffffff00;
    background: linear-gradient(to right, ${colors.darkGray}55, ${colors.gray}55);
    background-size: 120% 120%;
    border-radius: 5px;
  }
  
  @keyframes bg-animation {
    0%{background-position:0% 50%}
    50%{background-position:100% 0%}
    100%{background-position:0% 50%}
  }
}

`