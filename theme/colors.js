/**
  Colors

  Obs: Mostly of the color variables names are from aproximates names.
  Reference: https://www.htmlcsscolor.com
**/

let colors = {
  red: '#a61827',
  darkRed: '#65141c',
  white: '#ffffff',

  gray: '#dddddd',
  darkGray: '#222222',

  yellow: '#ebb33d',
  green: '#20E319',
}

export default {
  ...colors,

  main: {
    primary: colors.red,
    secundary: colors.white,

    background: colors.red,
  },

  dark: {
    primary: colors.red,
    secundary: colors.darkGray,

    background: colors.darkGray,
  }
};
