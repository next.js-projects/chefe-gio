import alegreya from './alegreya-sans-sc-regular.ttf'
import amsterdam from './amsterdam-two-ttf.ttf'

/*
  To add any font into the project:

  {
    name: "Name of Font",
    url: "font url imported",
  }
  
*/
export default [
  {
    name: "Alegreya Sans",
    url: alegreya,
  },
  {
    name: "Amsterdam Two",
    url: amsterdam,
  },
];
